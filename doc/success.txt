Loading 320 conformers
clustering by total energy
Number of clusters is 18
log 10 ligands. For change this number set the -n parameter

Rate Sucess of Best Energy:
    File	               T.Energy	             I.Energy 	           RMSD
mzm_ref_run_25.log	              -15.746	             -21.667	          1.397
mzm_ref_run_26.log	              -15.729	             -21.649	          1.400
mzm_ref_run_11.log	              -15.717	             -21.639	          1.400
mzm_ref_run_10.log	              -15.717	             -21.637	          1.383
mzm_ref_run_24.log	              -15.716	             -21.637	          1.404
mzm_ref_run_17.log	              -15.714	             -21.635	          1.398
mzm_ref_run_32.log	              -15.713	             -21.635	          1.406
mzm_ref_run_27.log	              -15.709	              -21.63	          1.399
mzm_ref_run_19.log	              -15.707	             -21.628	          1.393
mzm_ref_run_7.log	              -15.692	             -21.613	          1.397
mzm_ref_run_3.log	              -15.682	             -21.599	          1.385
mzm_ref_run_15.log	              -15.675	             -21.596	          1.409
mzm_ref_run_18.log	               -15.67	             -21.589	          1.390
mzm_ref_run_13.log	              -15.669	              -21.59	          1.393
mzm_ref_run_9.log	              -15.668	             -21.587	          1.412
mzm_ref_run_2.log	              -15.667	             -21.591	          1.389
mzm_ref_run_14.log	              -15.666	             -21.585	          1.408
mzm_ref_run_16.log	              -15.655	             -21.573	          1.408
mzm_ref_run_23.log	              -15.654	              -21.57	          1.379
mzm_ref_run_29.log	              -15.623	             -21.541	          1.432
mzm_ref_run_28.log	              -15.607	             -21.529	          1.434
mzm_ref_run_12.log	              -15.598	              -21.52	          1.389
mzm_ref_run_5.log	              -15.564	             -21.483	          1.413
mzm_ref_run_8.log	              -15.557	             -21.475	          1.402
mzm_ref_run_1.log	              -15.542	             -21.456	          1.410
mzm_ref_run_31.log	              -15.521	             -21.446	          1.444
mzm_ref_run_21.log	              -15.471	             -21.378	          1.414
mzm_ref_run_20.log	              -15.469	             -21.377	          1.407
mzm_ref_run_30.log	              -15.453	             -21.376	          1.433
mzm_ref_run_4.log	              -15.337	             -21.262	          1.476
mzm_ref_run_6.log	              -15.178	              -21.12	          1.524
mzm_ref_run_22.log	              -14.893	             -20.558	          5.848
sucess rate with best energy criterion is 96.88% and has RMSD of 1.40

Rate Sucess of Low RMSD:
    File	               T.Energy	             I.Energy 	           RMSD
mzm_ref_run_23.log	              -15.654	              -21.57	          1.379
mzm_ref_run_10.log	              -15.717	             -21.637	          1.383
mzm_ref_run_3.log	              -15.682	             -21.599	          1.385
mzm_ref_run_12.log	              -15.598	              -21.52	          1.389
mzm_ref_run_2.log	              -15.667	             -21.591	          1.389
mzm_ref_run_18.log	               -15.67	             -21.589	          1.390
mzm_ref_run_19.log	              -15.707	             -21.628	          1.393
mzm_ref_run_13.log	              -15.669	              -21.59	          1.393
mzm_ref_run_7.log	              -15.692	             -21.613	          1.397
mzm_ref_run_25.log	              -15.746	             -21.667	          1.397
mzm_ref_run_17.log	              -15.714	             -21.635	          1.398
mzm_ref_run_27.log	              -15.709	              -21.63	          1.399
mzm_ref_run_11.log	              -15.717	             -21.639	          1.400
mzm_ref_run_26.log	              -15.729	             -21.649	          1.400
mzm_ref_run_8.log	              -15.557	             -21.475	          1.402
mzm_ref_run_24.log	              -15.716	             -21.637	          1.404
mzm_ref_run_32.log	              -15.713	             -21.635	          1.406
mzm_ref_run_20.log	              -15.469	             -21.377	          1.407
mzm_ref_run_16.log	              -15.655	             -21.573	          1.408
mzm_ref_run_14.log	              -15.666	             -21.585	          1.408
mzm_ref_run_15.log	              -15.675	             -21.596	          1.409
mzm_ref_run_1.log	              -15.542	             -21.456	          1.410
mzm_ref_run_9.log	              -15.668	             -21.587	          1.412
mzm_ref_run_5.log	              -15.564	             -21.483	          1.413
mzm_ref_run_21.log	              -15.471	             -21.378	          1.414
mzm_ref_run_29.log	              -15.623	             -21.541	          1.432
mzm_ref_run_30.log	              -15.453	             -21.376	          1.433
mzm_ref_run_28.log	              -15.607	             -21.529	          1.434
mzm_ref_run_31.log	              -15.521	             -21.446	          1.444
mzm_ref_run_4.log	              -15.337	             -21.262	          1.476
mzm_ref_run_6.log	              -15.178	              -21.12	          1.524
mzm_ref_run_22.log	              -14.413	             -19.759	          2.001
sucess rate with low RMSD criterion is 96.88% and has RMSD of 1.38
End.
