**DTSTATISTIC**   

Installation:  
`> git clone https://gitlab.com/aaronleao/dtstatistic`  
`> cd dtstatistic`  
`> cd build`  
`> make`

This is the repository to **dtstatistic** project. This repo is divided as follows:  

|**FOLDER**| **Description**|
|--------- |---------------------------------------------------|
|**src** | Folder containing all sources files *.h* and *.cpp*.|
|**build**| Folder containing Makefile.|
|**python**| Folder containing former version of dtstatistic.|
|**doc**| Under construction manual and others documents.|
|**profiling**| Will be removed. A perfilators log.|


  
:flag_br:
