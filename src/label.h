#include <vector>
#include <iostream>

namespace labels
{
std::vector<std::string> * get_labels(std::vector<std::string>* v);
std::vector<std::string> * get_log_files_matching_label(std::string label, std::vector<std::string>* v);	
}
