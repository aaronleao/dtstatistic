#include "label.h"

std::vector<std::string>* labels::get_labels(std::vector<std::string>* v)
{
	std::vector<std::string> *labels = new std::vector<std::string>;
	int pos = (*v)[0].find("_run_");
	std::string aux = (*v)[0].substr(0,pos);
	labels->push_back(aux);

	unsigned int counter = 0;
	for(unsigned int i=0; i< v->size(); i++)
	{
		pos = (*v)[i].find("_run_");
		aux = (*v)[i].substr(0,pos);
		for(unsigned int j=0; j< labels->size(); j++)
		{
			if((*labels)[j].compare(aux))
				counter++;
			else
			{
				counter = 0;
				continue;
			}
		}
		if(counter == labels->size())
		{
			labels->push_back(aux);
			counter=0;
		}
	}
	return labels;
}

std::vector<std::string>* labels::get_log_files_matching_label(std::string label, std::vector<std::string>* v)
{
	std::vector<std::string> *log_files = new std::vector<std::string>;
	int pos;
	std::string aux;

	for(unsigned int i=0; i< (*v).size(); i++)
	{
		pos = (*v)[i].find("_run_");
		aux = (*v)[i].substr(0,pos);
		if( !label.compare( aux ) )
		{
			log_files->push_back((*v)[i]);
		}
	}
	return log_files;
}

