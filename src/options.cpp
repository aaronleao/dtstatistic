#include "options.h"

options::options()
{
	this->label_flag = false;
	this->directory_flag = false;
	this->label_name="";
	this->directory_name="";
	this->output_name="dtstatistic.log";
	this->output_pdb="";
	this->output_flag=false;
	this->nruns=1;
	this->number_of_ligands=10;
	this->rmsd_clustering=2.0;
	this->analysis_by_total_energy=true;
	this->analysis_by_internal_energy=false;
	this->analysis_by_interaction_energy=false;
	this->print_help=false;
	this->only_rmsd_calculation=false;
	this->rmsd_file1="";
	this->rmsd_file2="";
	this->vs_flag=false;
	this->vs_top_files="";
	this->dir=NULL;
	this->labels=NULL;
	this->supress_clustering = false;
	this->success_rate_flag  = false;
	this->success_rmsd       = 2.0f;
	this->rmsd_reference_flag= false;
	this->reference_molecule = NULL;
	//this->leader_of_each_run = NULL;
	this->all_log_files					  =	new std::vector < std::vector< std::string>*>;
	this->all_molecules_separated_by_runs = new std::vector < std::vector < std::vector < molecule::molecule*>*>*>;
	this->all_molecules_joined_by_runs    = new std::vector < std::vector < molecule::molecule*>*>;
	this->leaders						  = new std::vector < std::vector < molecule::molecule*>*>;
}