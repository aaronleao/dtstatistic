#include <vector>
#include "molecule.h"
#include "rmsd.h"

namespace clustering
{
std::vector<molecule::molecule*> *clustering(std::vector<molecule::molecule*> *molecules , float rmsd_clustering, unsigned int number_of_ligands);
}
