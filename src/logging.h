#include <fstream>
#include <iomanip>
#include "molecule.h"
#include "options.h"
#include "sort.h"
namespace logging
{
void write_output_clustering(std::vector<molecule::molecule*> *leaders, std::string output_name, unsigned int number_of_ligands);
void write_pdb_file(const    std::vector<molecule::molecule*> *leaders, const std::string output_name);	
void write_output_success(std::vector<molecule::molecule*> leaders, std::string output_name, float success_rmsd);
}
