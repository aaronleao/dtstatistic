#include <cmath>
#include "molecule.h"

namespace rmsd
{
float getRMSD(molecule::molecule * i, molecule::molecule * j);
float getRMSD( std::string file_name1, std::string file_name2);
void getRMSD_reference_molecule( std::vector <molecule::molecule *> *molecules, molecule::molecule *reference);
}
