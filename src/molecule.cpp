#include "molecule.h"
#include "unistd.h"

molecule::atom::atom(){}
molecule::molecule::molecule()
{
    id=0;
    log_file_name="";
    pdb_file_name="";
    total_energy=0;
    internal_energy=0;
    interaction_energy=0;
    coulomb=0;
    vdW=0;
    rmsd_leader=0;
    number_of_heavy_atoms = 0;
    run = 0;
    number_of_molecules_in_cluster = 0;

}
int molecule::molecule::get_id(){	return id;}
std::string molecule::molecule::get_log_file_name(){	return log_file_name;}
std::string molecule::molecule::get_pdb_file_name(){	return pdb_file_name;}
float molecule::molecule::get_total_energy(){	return total_energy;}
float molecule::molecule::get_internal_energy(){   return internal_energy;}
float molecule::molecule::get_interaction_energy(){	return interaction_energy;}
float molecule::molecule::get_coulomb(){	return coulomb;}
float molecule::molecule::get_vdW(){	return vdW;}
float molecule::molecule::get_rmsd_leader(){    return rmsd_leader;}
unsigned short molecule::molecule::get_number_of_heavy_atoms(){return number_of_heavy_atoms;}
float molecule::molecule::get_rmsd_to_reference(){return rmsd_to_reference;}
float molecule::molecule::get_score(){return score;}
int molecule::molecule::get_run(){return run;}

void molecule::molecule::set_id(int var){id = var;}
void molecule::molecule::set_log_file_name(std::string var){log_file_name=var;}
void molecule::molecule::set_pdb_file_name(std::string var){pdb_file_name=var;}
void molecule::molecule::set_total_energy(float var){total_energy=var;}
void molecule::molecule::set_internal_energy(float var){internal_energy=var;}
void molecule::molecule::set_interaction_energy(float var){interaction_energy=var;}
void molecule::molecule::set_coulomb(float var){coulomb=var;}
void molecule::molecule::set_vdW(float var){vdW=var;}
void molecule::molecule::set_rmsd_leader(float var){rmsd_leader=var;}
void molecule::molecule::set_rmsd_to_reference(float var){rmsd_to_reference = var;}
void molecule::molecule::set_score(float a0, float a1, float a2){score = a0*vdW + a1*coulomb + a2;}
void molecule::molecule::set_run(int var){run = var;}



std::vector <molecule::molecule*>* molecule::load_molecules(std::string log_file_name)
{
    std::vector <molecule*> *molecules_vector = new std::vector <molecule*>;
 
    std::ifstream log_file;
    std::ifstream pdb_file;
    std::string pdb_file_name = log_file_name.substr(0,log_file_name.size() -3);
    pdb_file_name += "pdb";
    molecule * new_molecule;
    std::string aux;
    
    /*
    *   Open log and pdb files
    */
    log_file.open(log_file_name.c_str(), std::fstream::in);
    pdb_file.open(pdb_file_name.c_str(), std::fstream::in);

    /*
    * log file variables
    */
    int id;
    int number_of_members;
    float total_Energy;
    float internal_Energy;
    float coulomb;
    float vdw;
    float rmsd;
    int number_of_Clusters;
    int seed;

   if(log_file.is_open() && pdb_file.is_open())
   {
        log_file>>aux>>aux>>number_of_Clusters;//Geting number of clusters of the first line
        log_file>>aux>>aux>>seed;            
        for(int i=1 ; i <= number_of_Clusters ; i++)
        {

            /*
            * Molecule energy informations from .log
            */
            getline(log_file,aux);
            log_file>>aux>>id>>aux;
            getline(log_file,aux);
            log_file>>aux>>aux>>number_of_members;
            log_file>>aux>>aux>>total_Energy;
            log_file>>aux>>aux>>vdw;
            log_file>>aux>>aux>>coulomb;
            log_file>>aux>>aux>>internal_Energy;
            log_file>>aux>>aux>>rmsd;
            getline(log_file,aux);
            getline(log_file,aux);
            new_molecule = new molecule;
            new_molecule->set_id(id);
            new_molecule->set_log_file_name(log_file_name);
            new_molecule->set_pdb_file_name(pdb_file_name);
            new_molecule->set_total_energy(total_Energy);
            new_molecule->set_internal_energy(internal_Energy);
            new_molecule->set_interaction_energy(coulomb + vdw);
            new_molecule->set_vdW(vdw);
            new_molecule->set_coulomb(coulomb);
            new_molecule->set_score(0.1052, 0.0207, -6.6432); //score = (coeff_vdw * self.vdw) + (coeff_coul * self.coulomb) + c0
            new_molecule->set_run(extract_run(log_file_name));

            /*
            *           Atoms list from .pdb file
            */
            std::getline(pdb_file,aux);
            while( aux.compare("ENDMDL") )
            {
                std::getline(pdb_file,aux);
                if(aux.compare("ENDMDL") )
                {
                    new_molecule->atom_list.push_back(read_atom(aux, new_molecule));
                }
            }
            molecules_vector->push_back(new_molecule);

        }
    }
    else
    {
        std::cerr << "[DTSTATISTIC] Could not open neither " << log_file_name << " or " << pdb_file_name <<std::endl;
        exit(-1);
    }
    log_file.close();
    pdb_file.close();

    return molecules_vector;
}


molecule::molecule* molecule::molecule_from_pdb(std::string  pdb_file_name)
{
    /*
    *   Open log and pdb files
    */
    std::ifstream pdb_file;
    pdb_file.open(pdb_file_name.c_str(), std::fstream::in);
    molecule * new_molecule = new molecule;
    std::string line;// Garbage string
    std::string aux;
    /*
    * pdb files variables
    */
    
    if(pdb_file.is_open())
    {
        while(std::getline(pdb_file,line))
        {
            aux = line.substr(0,6);
            if(!aux.compare("ATOM  ") || !aux.compare("HETATM"))
            {
                new_molecule->atom_list.push_back(read_atom(line, new_molecule));                
            }

        }
    }
    else
    {
        std::cerr << "[DTSTATISTIC] Could not open " << pdb_file_name << "file." <<std::endl;
        exit(-1);
    }
    pdb_file.close();
    return new_molecule;
}

molecule::atom * molecule::read_atom(std::string line, molecule * new_molecule)
{
    atom * new_atom = new atom;

    new_atom->set_record_name(line.substr(0,6));
    new_atom->set_serial_number(stoi(line.substr(6,5)));
    new_atom->set_atom_name(line.substr(11,5));
    new_atom->set_residue_name(line.substr(16,4));
    new_atom->set_chain_id(line.substr(20,2));
    new_atom->set_residue_sequence(stoi(line.substr(22,4)));
    new_atom->set_x(stof(line.substr(26,12)));
    new_atom->set_y(stof(line.substr(38,8)));
    new_atom->set_z(stof(line.substr(46,8)));
    new_atom->set_occupancy(stof(line.substr(54,6)));
    new_atom->set_temperature_factor(stof(line.substr(60,6)));
    new_atom->set_element(line.substr(66,12));
    new_atom->set_charge(line.substr(78,2));
    if(new_atom->get_element().compare("           H")) // If currenty atom is not H.
    {
        new_molecule->set_number_of_heavy_atoms(); // Increase the number of heavy atoms.
    }    
    return new_atom;
}

int molecule::extract_run(std::string file_name)
{
    unsigned int pos = file_name.find("_run_");
    std::string aux = file_name.substr(pos+5, file_name.size());
    pos  = aux.find(".log");
    return stoi(aux.substr(0,pos));
}


