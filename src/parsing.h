#ifndef parsing
#define parsing

#include "options.h"
#include <string>
#include <cstring>
#include <iostream>
#include <cstdlib>
#include "directory.h"


namespace parse
{
void parse( int argc , char * argv[] ,options *opt);
void print_help();
}


#endif
