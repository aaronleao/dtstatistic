#include "logging.h"

void logging::write_output_clustering(std::vector<molecule::molecule*> *leaders, std::string output_name, unsigned int number_of_ligands)
{
	std::ofstream output_file;
	output_file.open(output_name.c_str(),std::fstream::out);
	if(number_of_ligands >= leaders->size())
		number_of_ligands = leaders->size();
	if(output_file.is_open())
	{
		//output_file << std::setw(24) << std::setw(26) << std::setw(7) << std::setw(7) << std::setw(7);
 		output_file << "\tFile\t\t\t" << "Model\t" << "T_Energy \t" << "I_Energy \t" << "RMSD\t" << "#Molecules"<<std::endl;
		for( unsigned int i=0 ; i < number_of_ligands ; i++)
		{
			output_file << (*leaders)[i]->get_log_file_name() << "\t\t" << (*leaders)[i]->get_id() << "\t" << (*leaders)[i]->get_total_energy() << "\t\t" << (*leaders)[i]->get_interaction_energy() << "\t\t" << (*leaders)[i]->get_rmsd_leader()<< "\t\t" << (*leaders)[i]->get_number_of_molecules_in_cluster() << std::endl;
		}
		std::cerr << "[DTSTATISTIC] Logged successfully in file " << output_name << "." << std::endl;
	}	
	output_file.close();
}

void logging::write_pdb_file(const std::vector<molecule::molecule*> *leaders, const std::string output_name)
{
	std::ofstream output_file;
	output_file.open(output_name.c_str());
	
	if(output_file.is_open())
	{
		for( unsigned int i=0 ; i < leaders->size() ; i++)
		{
			output_file << "MODEL " << i+1<<std::endl;
			molecule::molecule * aux = (*leaders)[i];
			for(unsigned int j=0 ; j< aux->atom_list.size() ; j++)
			{
				molecule::atom *new_atom = aux->atom_list[j];
				output_file<<\
			    std::setw(6)<<\
			    new_atom->get_record_name() <<\
			    std::setw(5)<<\
			    new_atom->get_serial_number()<<\
			    std::setw(5)<<\
			    new_atom->get_atom_name() <<\
			    std::setw(4)<<\
			    new_atom->get_residue_name()<<\
			    std::setw(2)<<\
			    new_atom->get_chain_id()<<\
			    std::setw(4)<<\
			    new_atom->get_residue_sequence()<<\
			    std::setw(12)<<std::setprecision(3)<<\
			    new_atom->get_x()<<\
			    std::setw(8)<<std::setprecision(3)<<std::fixed<<\
			    new_atom->get_y()<<\
			    std::setw(8)<<std::setprecision(3)<<std::fixed<<\
			    new_atom->get_z()<<\
			    std::setw(6)<<\
			    new_atom->get_occupancy()<<\
			    std::setw(6)<<\
			    new_atom->get_temperature_factor()<<\
			    std::setw(12)<<\
			    new_atom->get_element()<<\
			    std::setw(2)<<\
			    new_atom->get_charge()<<std::endl;
			}
			output_file << "ENDMDL" <<std::endl;
		}
	}	
	output_file.close();
}



void logging::write_output_success(std::vector<molecule::molecule*> leaders, std::string output_name, float success_rmsd)
{
	std::ofstream output_file;
	output_file.open(output_name.c_str(),std::fstream::out);

	if(output_file.is_open())
	{
		for(char t = 1; t<=6 ; t++)
		{
			float count = 0;
			sort::sort_molecule(&leaders , t);
			output_file << "\tFile\t\t" << "Model\t" << "Total Energy \t" \
						<< "Internal Energy \t" << "Interaction Energy\t" \
						<< "Score\t\t" <<"RMSD\t\t" << "#Molecules"<<std::endl;
			for( unsigned int i=0 ; i < leaders.size() ; i++)
			{
				if(leaders[i]->get_rmsd_to_reference() <= success_rmsd)
				{
					output_file << leaders[i]->get_log_file_name() << "\t" \
								<< leaders[i]->get_id() << "\t" \
								<< std::setw(8)\
								<< leaders[i]->get_total_energy() << "\t\t" \
								<< std::setw(8)\
								<< leaders[i]->get_internal_energy() << "\t\t" \
								<< std::setw(8)\
								<< leaders[i]->get_interaction_energy() << "\t\t\t" \
								<< std::setw(8)\
								<< leaders[i]->get_score() << "\t" \
								<< std::setw(8)\
								<< leaders[i]->get_rmsd_to_reference()<< "\t" \
								<< std::setw(8)\
								<< leaders[i]->get_number_of_molecules_in_cluster() << std::endl;
					count++;
				}

			}
			std::string option;
			switch(t)
			{
				case 1:
					option = " Total Energy ";break;
				case 2:
					option = " Interaction Energy ";break;
				case 3:
					option = " Internal Energy ";break;
				case 4:
					option = " Score ";	break;
				case 5:
					option = " RMSD ";break;
				case 6:
					option = " #Molecules ";break;
			}
			output_file << "Sorting by "<< option << (count/(leaders.size()))*100 <<"% of accuracy" << std::endl;
		}

		std::cerr << "[DTSTATISTIC] Logged successfully in file " << output_name << "." << std::endl;
	}
	else
	{
		std::cerr << "[DTSTATISTIC] Unable to open file" << output_name << "." << std::endl;
	}
	output_file.close();
}
