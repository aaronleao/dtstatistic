#include "directory.h"

DIR * directory::check_directory(std::string path_name)
{
	DIR *dir=opendir(path_name.c_str());
	if(!dir)
    {
        std::cerr<<"[DTSTATISTIC] Cannot access directory "<<path_name<<"."<<std::endl;
        exit(1);
    }
    else
    {   
        return dir;
    }
}

bool directory::check_file(std::string file_name)
{
    std::ifstream file;
    file.open(file_name.c_str());
    if(file.is_open())
    {
        file.close();
        return true;
    }
    std::cerr << "[DTSTATISTIC] File not found: " << file_name;
    exit(0);
    return false;
}


void directory::get_file_list(DIR * dir, std::string label_name, std::vector<std::string> *log_files)
{
    struct dirent *dirent;
    std::string file_name;
    int size = label_name.size();
    DIR *dira=opendir(".");
    while ( (dirent = readdir(dira)))
    {
        file_name=dirent->d_name;
        std::string aux = file_name.substr(0,size);
        if(!aux.compare(label_name))
        {
            if(file_name.find(".log")!=std::string::npos)
            {
                log_files->push_back(file_name);
            }
        }

    }
    free(dira);

}


std::vector<std::string> * directory::list_log_files(DIR * directory)
{
    std::vector<std::string> *log_files =  new std::vector<std::string>;
    std::string file_name;
    struct dirent *dira;

    while ( (dira = readdir(directory)) )
    {
        file_name=dira->d_name;
        if(file_name.find("_run_") != std::string::npos && file_name.find(".log") != std::string::npos)
        {
            log_files->push_back(file_name);
        }
    }
    free(dira);

    if(!log_files->size())
    {
        std::cerr << "[DTSTATISTIC] No log files found at directory." << std::endl;
        exit(-1);
    }
        
    return log_files;
}