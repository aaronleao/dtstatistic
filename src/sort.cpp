#include "sort.h"

bool sort::sort_by_total_energy(molecule::molecule *i , molecule::molecule *j)
{
	return i->get_total_energy() < j->get_total_energy() ;
}

bool sort::sort_by_interaction_energy(molecule::molecule *i , molecule::molecule *j)
{
	return i->get_interaction_energy() < j->get_interaction_energy();
}

bool sort::sort_by_rmsd(molecule::molecule *i , molecule::molecule *j)
{
	return i->get_rmsd_to_reference() < j->get_rmsd_to_reference();
}

bool sort::sort_by_internal_energy(molecule::molecule *i , molecule::molecule *j)
{
	return i->get_internal_energy() < j->get_internal_energy();
}

bool sort::sort_by_score(molecule::molecule *i , molecule::molecule *j)
{
	return i->get_score() < j->get_score();
}


bool sort::sort_by_number_of_molecules_in_cluster(molecule::molecule *i , molecule::molecule *j)
{
	return i->get_number_of_molecules_in_cluster() < j->get_number_of_molecules_in_cluster();
}

void sort::sort_molecule(std::vector<molecule::molecule*> *aux , char option)
{

	if(option == 1)
	{
		std::sort(aux->begin(),aux->end(),sort_by_total_energy);
	}
	if(option == 2)
	{
		std::sort(aux->begin(),aux->end(),sort_by_interaction_energy);
	}
	if(option == 3)
	{
		std::sort(aux->begin(),aux->end(),sort_by_internal_energy);
	}
	if(option == 4)
	{
		std::sort(aux->begin(),aux->end(),sort_by_score);
	}
	if(option == 5)
	{
		std::sort(aux->begin(),aux->end(),sort_by_rmsd);	
	}
	if(option == 6)
	{
		std::sort(aux->begin(),aux->end(),sort_by_number_of_molecules_in_cluster);	
	}
}

