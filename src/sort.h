#include <vector>
#include "molecule.h"
#include <algorithm>

namespace sort
{
bool sort_by_total_energy(molecule::molecule *i , molecule::molecule *j);
bool sort_by_interaction_energy(molecule::molecule *i , molecule::molecule *j);
bool sort_by_internal_energy(molecule::molecule *i , molecule::molecule *j);
bool sort_by_score(molecule::molecule *i , molecule::molecule *j);
bool sort_by_rmsd(molecule::molecule *i , molecule::molecule *j);
bool sort_by_number_of_molecules_in_cluster(molecule::molecule *i , molecule::molecule *j);
void sort_molecule(std::vector<molecule::molecule *> *aux, char option);
}

