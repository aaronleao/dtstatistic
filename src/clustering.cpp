#include "clustering.h"


std::vector<molecule::molecule*> *clustering::clustering(std::vector<molecule::molecule*> *molecules , float rmsd_clustering, unsigned int number_of_ligands)
{
	/*
	*	Pushing back the best molecule in leaders
	*/

	std::vector<molecule::molecule*> *leaders = new std::vector<molecule::molecule*>;
	leaders->push_back((*molecules)[0]);
	(*leaders)[0]->set_rmsd_leader(0);


	unsigned int molecules_max =molecules->size();	
	unsigned int i=0,j=0;
	float rmsd;
	unsigned int count=0;


	for(i=1; i< molecules_max ; i++)
	{
		if(leaders->size() < number_of_ligands)
		{
			count = 0;
			for(j=0; j < leaders->size() ; j++)
			{
				rmsd = rmsd::getRMSD((*molecules)[i],(*leaders)[j]);
				if(rmsd >= rmsd_clustering )
				{
					
					count ++;
					if(count==1)
					{
						(*molecules)[i]->set_rmsd_leader(rmsd);
					}
				}
				else
					(*leaders)[j]->set_number_of_molecules_in_cluster();

			}
			if(count == leaders->size())
			{
				leaders->push_back((*molecules)[i]);
			}
		}

	}

	return leaders;
}