#ifndef OPTIONS_H
#define OPTIONS_H

#include <stdio.h>
#include <dirent.h>
#include <iostream>
#include <vector>
#include <cstdlib>
#include "molecule.h"

class options
{
public:
	bool label_flag;
	bool directory_flag;
	bool output_flag;
	bool analysis_by_total_energy;
	bool analysis_by_internal_energy;
	bool analysis_by_interaction_energy;
	bool only_rmsd_calculation;
	bool print_help;
	bool supress_clustering;
	bool success_rate_flag;
	bool rmsd_reference_flag;

	std::string label_name;
	std::string directory_name;
	std::string output_name;
	std::string output_pdb;
	std::string rmsd_file1;
	std::string rmsd_file2;
	std::string reference_file;

	unsigned int number_of_ligands;
	float rmsd_clustering;
	DIR * dir;
	float success_rmsd;
	molecule::molecule * reference_molecule;

	
	std::vector < std::string>*	 					    				directory_log_files;			// All .log files found in directory 
	std::vector < std::string>* 										labels;							// All Labels
	std::vector < std::vector <std::string>*>*							all_log_files;					// All .log files separated by labels
	std::vector < std::vector <std::vector < molecule::molecule*>*>*>*	all_molecules_separated_by_runs;// All molecules separated by run which is separated by labels
	std::vector < std::vector <molecule::molecule*>*>*	 				all_molecules_joined_by_runs;	// All molecules separated by run which is separated by labels
	std::vector < std::vector <molecule::molecule*>*>*  				leaders;

	// std::vector < molecule::molecule*>*					leader_of_each_run;
	
	bool vs_flag;
	std::string vs_top_files;
	int nruns;

	options();




};


#endif