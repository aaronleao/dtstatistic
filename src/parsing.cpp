#include "parsing.h"

void parse::parse( int argc , char * argv[] , options *opt)
{
	
	std::string aux;
	for(int i=1;i<argc;i++)
	{
		aux=argv[i];
		if(! aux.compare("-h") || argc==1)
		{
			opt->print_help=true;
			print_help();
		}

		if(! aux.compare("-l") )
		{
			if(i+1<argc)
			{
				opt->label_flag = true;
				opt->label_name=argv[i+1];
				i++;
				continue;
			}
			else
				print_help();
		}

		if(! aux.compare("-d") )
		{
			if(i+1<argc)
			{
				opt->directory_flag = true;
				opt->directory_name=argv[i+1];	
				i++;
				continue;
			}
			else
				print_help();
		}

		if(! aux.compare("-o"))
		{
			if(i+1<argc)
			{
				opt->output_flag = true;
				opt->output_name = argv[i+1];
				i++;
				continue;
			}
			else
				print_help();
		}

		if(! aux.compare("-n") )
		{
			if(i+1<argc)
			{
				opt->number_of_ligands=atoi(argv[i+1]);	
				i++;
				continue;
			}
			else
				print_help();
		}

		if(! aux.compare("-t"))
		{
			opt->analysis_by_total_energy=true;
			opt->analysis_by_internal_energy=false;
			continue;
		}

		if(! aux.compare("-i"))
		{
			opt->analysis_by_total_energy=false;
			opt->analysis_by_internal_energy=true;
			continue;
		}

		if(!aux.compare("-c"))
		{
			if(i+1<argc)
			{
				opt->rmsd_clustering=atof(argv[i+1]);
				i++;
				continue;
			}
			else
				print_help();		
		}

		if(!aux.compare("-g"))
		{
			if(i+2 < argc)
			{
				opt->only_rmsd_calculation = true;
				opt->rmsd_file1 = argv[i+1];
				opt->rmsd_file2 = argv[i+2];
				i++;i++;
				continue;
			}
			else
				print_help();
		}

		if(!aux.compare("-sc"))
		{
			opt->supress_clustering = true;
			continue;
		}

		if(!aux.compare("-r"))
		{
			if(i+1 < argc)
			{
				opt->rmsd_reference_flag = true;
				opt->reference_file = argv[i+1];
				i++;
				continue;
			}
			else
				print_help();
		}

		if(!aux.compare("-s"))
		{
			if(i+1 < argc)
			{
				opt->success_rate_flag  = true;
				opt->success_rmsd = atof(argv[i+1]);
				i++;
				continue;
			}
			else
				print_help();
		}		

		if(!aux.compare("-vs"))
		{
			if(i+1 < argc)
			{
				opt->vs_flag = true;
				opt->vs_top_files = argv[i+1];
				i++;
				continue;
			}
			else
				print_help();
		}

		else
		{
			std::cerr<<"[DTSTATISTIC] Unknown parameter "<<aux<<std::endl;
			exit(1);
		}
	}
}

void parse::print_help()
{
std::cout<<"\
[DTSTATISTIC] Help message\n\n\
Usage: dtstatistic -l <LIGAND_LABEL> -d <DIR> -i  -n 20 -o my_output\n\
Parameters: \n\
  -h          		show this help message and exit\n\
  -l name     		set ligand label\n\
  -d name     		set directory name\n\
  -t          		analysis by total energy\n\
  -i          		analysis by interaction energy\n\
  -o name     		output file label (out)\n\
  -n number   		number of poses to log (10)\n\
  -c number   		clustering rmsd (2.0)\n\
  -g ref1.pdb ref2.pdb 	RMSD between ref1.pdb and ref2.pdb\n";

	exit(0);
}	
