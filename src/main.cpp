#include <cstdlib>
#include "options.h"
#include "parsing.h"
#include "rmsd.h"
#include "directory.h"
#include "label.h"
#include "sort.h"
#include "clustering.h"
#include "logging.h"


/***********************************************************************************
									GLOBAL VARIABLES
***********************************************************************************/
options opt;




int main(int argc, char * argv[])
{
	/* 
	* Parsing input command
	*/
	parse::parse(argc, argv, &opt);
	
	/*
	* Checking flags
	*/
	if(!opt.directory_flag && !opt.label_flag && !opt.only_rmsd_calculation)
	{
		std::cerr << "[DTSTATISTIC] Either -g or -d and/or -l must be used." << std::endl;
		parse::print_help();
		exit(-1);
	}
	if(!opt.output_flag)
	{
		if(!opt.label_flag)
		{
			opt.output_name = "dtstatistic.log";	
		}
		else
		{
			opt.output_name = "dts" + opt.label_name + ".log";	
		}

	}

	/*
	* Check -d flag and change to the workpath to it.
	*/
	if(opt.directory_flag)
		opt.dir = directory::check_directory(opt.directory_name);
	else
	{
		opt.directory_name = ".";
		opt.dir = directory::check_directory(opt.directory_name);
	}

	/*
	* Saving former workpath and changing to -d 
	*/
	char former_path[10000];
	char * aux1 = getcwd(former_path, sizeof(former_path));
	if(!aux1)
	{
		exit(0);
	}
	int aux2 = chdir(opt.directory_name.c_str());
	if(aux2==10)
		exit(0);

	/*
	* Checking -g flag:	Calculation of RMSD
	*/
	if(opt.only_rmsd_calculation)
	{
		directory::check_file(opt.rmsd_file1);
		directory::check_file(opt.rmsd_file2);
		rmsd::getRMSD( opt.rmsd_file1, opt.rmsd_file2 );

	}

	/*
	* List the all log files found in directory
	*/
	opt.directory_log_files = directory::list_log_files(opt.dir);	

	/*
	* List all distincts labels from all log files availables
	*/
	opt.labels = labels::get_labels(opt.directory_log_files);

	/*
	* Resizing all vectors used
	*/
	opt.all_log_files->resize(opt.labels->size());
	opt.all_molecules_separated_by_runs->resize(opt.labels->size());
	opt.all_molecules_joined_by_runs->resize(opt.labels->size());
	opt.leaders->resize(opt.labels->size());

	/*
	* Loading all molecules listed in opt.all_log_files
	* PS: This part of the code is very tricky to me, so it will be many comments followed by its command.
	*/
	for(unsigned int i=0; i< opt.labels->size(); i++)
	{
		// opt.all_log_files[i] = A vector of all runs to the label i
		(*opt.all_log_files)[i] = labels::get_log_files_matching_label((*opt.labels)[i], opt.directory_log_files);
		std::vector <std::vector <molecule::molecule*>*>* aux1 = new std::vector <std::vector <molecule::molecule*>*>;

		//For every label creates a new vector of runs
		(*opt.all_molecules_joined_by_runs)[i] = new std::vector<molecule::molecule*>;

		for(unsigned int j=0; j< (*opt.all_log_files)[i]->size() ; j++)
		{
			//aux2 = vector of all molecules of the run j.
			std::vector <molecule::molecule*>* aux2 = molecule::load_molecules((*(*opt.all_log_files)[i])[j]);

			//aux1 = vector of runs of label i
			aux1->push_back(aux2);

			unsigned int number_of_models = aux2->size();
			for(unsigned int k=0 ; k< number_of_models ;k++)
			{
				// Vector of all molecules of a label
				(*opt.all_molecules_joined_by_runs)[i]->push_back( (*aux2)[k] );
			}			
		}
		//opt.all_molecules_separated_by_runs = vector of all molecules of all j runs of all i labels
		(*opt.all_molecules_separated_by_runs)[i] = aux1;
	}

	/*
	* Sorting molecules by energy
	*/
	for(unsigned int i=0 ; i<opt.labels->size() ; i++ )//i runs in label size()
	{
			if(opt.analysis_by_total_energy)
				sort::sort_molecule((*opt.all_molecules_joined_by_runs)[i],1);
			if(opt.analysis_by_internal_energy)
				sort::sort_molecule((*opt.all_molecules_joined_by_runs)[i],2);
	}


	/*
	* Clustering leaders
	*/
	if(!opt.supress_clustering)
	{
		for(unsigned int i=0; i<opt.labels->size(); i++)
		{
			(*opt.leaders)[i] = clustering::clustering((*opt.all_molecules_joined_by_runs)[i] , opt.rmsd_clustering, opt.number_of_ligands);
		}
		/*
		*	Logging leader in output file
		*/
		for(unsigned int i=0; i<opt.labels->size(); i++)
		{
			if(!opt.output_flag)
				opt.output_name = "dts" + (*opt.labels)[i] + ".log";
			logging::write_output_clustering((*opt.leaders)[i], opt.output_name, opt.number_of_ligands);
			logging::write_pdb_file((*opt.leaders)[i], opt.output_pdb);
		}

	}
	// else
	// {
	// 	std::vector <molecule::molecule*> aux;
	// 	for(unsigned int i=0; i<opt.labels->size(); i++)
	// 	{
	// 		aux.push_back((*opt.all_molecules_joined_by_runs)[i][0]);
	// 	}
	// 	logging::write_output_clustering(aux, opt.output_name, opt.number_of_ligands);
	// }



	/*
	* Check -r to RMSD reference .pdb molecule
	*/
	if(opt.rmsd_reference_flag)
	{
		if(  !opt.label_flag)
		{
			std::cerr << "[DTSTATISTIC] Must be informed a label to sort with reference pdb file. " << std::endl;
			exit(-1);
		}
		directory::check_file(opt.reference_file);											// Check file
		opt.reference_molecule = molecule::molecule_from_pdb(opt.reference_file);			// Load reference molecule
		
		rmsd::getRMSD_reference_molecule( opt.leader_of_each_run, opt.reference_molecule);	// Sort by RMSD
		
		logging::write_output_success(*opt.leader_of_each_run, "dtsuccess.log", opt.success_rmsd);		
	}

	aux2 = chdir(former_path);




	// // /*
	// // *	Cleaning up memory
	// // */
	// // for(unsigned int i=0;i< molecules.size();i++)
	// // {
	// // 	for(unsigned int j=0; j< molecules[i]->atom_list.size();j++)
	// // 		delete molecules[i]->atom_list[j];	
	// // 	delete molecules[i];
	// // }
	// // delete leaders;

	// exit(0);
}
